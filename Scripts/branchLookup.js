﻿
function lookup()
{
    var request = new XMLHttpRequest();
    var id = document.getElementById("idinputbox").value;
    request.onreadystatechange = function () {
        if (this.readyState == 4) {
            var data = JSON.parse(this.response);

            if (!('StatusCode' in data) || !('Message' in data))
            {
                document.getElementById("status").innerText = "The server responded with a malformed response.";
                clearDisplay();
            }
            if (data.StatusCode == 404)
            {
                document.getElementById("status").innerText = "There are no branches with the number '" + id + "'.";
                clearDisplay();
            }
            if (data.StatusCode == 200 && 'Data' in data)
            {
                document.getElementById("status").innerText = "Displaying branch information for '"+id+"'.";
                for (var i in data.Data)
                {
                    setDisplay(data.Data[i])
                }
            }
        }
    };
    request.open("GET", "/Handlers/Branches.ashx?i=" + id, true);
    request.send();
    
    document.getElementById("status").innerText = "Querying data for '"+id+"'..."
}

function clearDisplay()
{
    document.getElementById("outputContainer").innerHTML = "";
}

function setDisplay(data)
{
    var container = document.getElementById("outputContainer");
    container.innerHTML = "";

    var NumberDisplay = document.createElement("div");
    NumberDisplay.innerText = "Number: " + data.Number;
    
    var NameDisplay = document.createElement("div");
    NameDisplay.innerText = "Name: " + data.Name;

    var SpecialismsDisplay = document.createElement("ul");
    for (var i in data.Specialisms)
    {
        var Specialism = data.Specialisms[i];

        var SpecialismItem = document.createElement("li");
        var SpecialismId = document.createElement("div");
        SpecialismId.innerText = "ID: " + Specialism.Id;

        var SpecialismName = document.createElement("div");
        SpecialismName.innerText = "Name: " + Specialism.Name;


        SpecialismItem.appendChild(SpecialismId);
        SpecialismItem.appendChild(SpecialismName);

        if (Specialism.Description != null)
        {
            var SpecialismDesc = document.createElement("div");
            SpecialismDesc.innerText = "Description: " + Specialism.Description;
            SpecialismItem.appendChild(SpecialismDesc);
        }

        SpecialismsDisplay.appendChild(SpecialismItem);
    }

    container.appendChild(NumberDisplay);
    container.appendChild(NameDisplay);
    container.appendChild(document.createElement("hr"));
    container.appendChild(SpecialismsDisplay);
}