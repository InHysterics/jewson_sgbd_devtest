﻿<%@ WebHandler Language="C#" Class="Branches" %>

using System;
using System.Web;

public class Branches : Handler_Data_Provider<Branch>
{
    public Branches() : base(App.BranchData) { }
}