﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Branches.aspx.cs" Inherits="_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <title>Branch Lookup</title>
    <link rel="stylesheet" type="text/css" href="branches.css" />
    <script src="/Scripts/branchLookup.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="base" Runat="Server">
    <div id="inputContainer" class="leftContent">
        <div id="status">Please enter an id to lookup.</div>
        <form onsubmit="lookup(); return false;">
            <input id="idinputbox" type="text" />
            <input type="button" onclick="lookup()" value="Lookup" /> <br />
        </form>
    </div>
    <div id="outputContainer" class="rightContent"></div>
</asp:Content>

