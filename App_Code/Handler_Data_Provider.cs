﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

public class Handler_Data_Provider<T> : IHttpHandler
{
    public readonly Data_Provider<T> provider;

    public Handler_Data_Provider(Data_Provider<T> provider)
    {
        this.provider = provider;
    }

    public void ProcessRequest(HttpContext context)
    {
        if (BeforeValidation(context)) return;
        Int32 id = -1;
        if (!String.IsNullOrWhiteSpace(context.Request["Number"])) Int32.TryParse(context.Request["Number"], out id);
        if (!String.IsNullOrWhiteSpace(context.Request["ID"])) Int32.TryParse(context.Request["ID"], out id);
        if (!String.IsNullOrWhiteSpace(context.Request["i"])) Int32.TryParse(context.Request["i"], out id);
        if (id == -1)
        {
            context.Response.ContentType = "text/json";
            context.Response.Write(new Response()
            {
                StatusCode = 400,
                Message = "Malformed request: Request lacks any of the following paremeters 'Number, ID, i'."
            });
            return;
        }
        if (!provider.ContainsKey(id))
        {
            context.Response.ContentType = "text/json";
            context.Response.Write(new Response()
            {
                StatusCode = 404,
                Message = "No results: There are no branches with the id '" + id + "'"
            });
            return;
        }
        if (AfterValidation(context)) return;
        if (BeforeResponseGeneration(context)) return;

        T data = provider[id];
        context.Response.ContentType = "text/json";
        context.Response.Write((Object)new Response_Data<T>(data)
        {
            StatusCode = 200,
            Message = "Data was retrieved successfuly"
        });

        if (AfterResponseGeneration(context)) return;
    }

    /// <summary>
    /// Occurs before validation occurs.
    /// </summary>
    /// <param name="context">The context for this session.</param>
    /// <returns>If true, halts execution at this point.</returns>
    public virtual Boolean BeforeValidation(HttpContext context)
    {
        return false;
    }
    /// <summary>
    /// Occurs after validation occurs.
    /// </summary>
    /// <param name="context">The context for this session.</param>
    /// <returns>If true, halts execution at this point.</returns>
    public virtual Boolean AfterValidation(HttpContext context)
    {
        return false;
    }
    /// <summary>
    /// Occurs before the response for this handler is generated.
    /// </summary>
    /// <param name="context">The context for this session.</param>
    /// <returns>If true, halts execution at this point.</returns>
    public virtual Boolean BeforeResponseGeneration(HttpContext context)
    {
        return false;
    }
    /// <summary>
    /// Occurs after the response for this handler is generated, this is the last opperation in the context of this handler.
    /// </summary>
    /// <param name="context">The context for this session.</param>
    /// <returns>If true, halts execution at this point.</returns>
    public virtual Boolean AfterResponseGeneration(HttpContext context)
    {
        return false;
    }
    

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }
}