﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

public class Response_Data<T> : Response
{
    public List<T> Data;
    public Response_Data(T data) : this(new List<T>() { data }) { }
    public Response_Data(List<T> data)
    {
        this.Data = data;
    }
}