﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web.Hosting;
using Newtonsoft.Json;

public abstract class Data_Provider<T> : Dictionary<Int32, T> 
{
    public Data_Provider(String filelocation)
    {
        using (StreamReader reader = new StreamReader(HostingEnvironment.MapPath(filelocation)))
            foreach (T obj in JsonConvert.DeserializeObject<T[]>(reader.ReadToEnd()))
                Add(Index(obj), obj);
    }

    protected abstract Int32 Index(T obj);
    protected abstract Boolean Filter(T obj, Dictionary<String, String> arguments);

    public List<T> GetData(Dictionary<String, String> arguments = null)
    {
        if (arguments == null) return this.Values.ToList();
        return this.Values.Where(x => Filter(x, arguments)).ToList();
    }
}