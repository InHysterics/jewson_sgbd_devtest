﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

public class Data_Provider_Branch : Data_Provider<Branch>
{
    private const String _BRANCH_DATA_LOCATION = "~/branches.json";

    public Data_Provider_Branch() : base(_BRANCH_DATA_LOCATION) { }

    protected override Int32 Index(Branch branch)
    {
        return branch.Number;
    }
    protected override Boolean Filter(Branch branch, Dictionary<String, String> arguments)
    {
        //Not implmented due to scope. Left in to allow swift implementation in the future.

        //Example implementation..
        if (!String.IsNullOrWhiteSpace(arguments["Name"]) && !branch.Name.ToLower().Contains(arguments["Name"].ToLower())) return false; //When the name argument is set, do not include branches not containing the provided string.

        return true;
    }
}