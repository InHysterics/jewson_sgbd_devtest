﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Web;

[StructLayout(LayoutKind.Sequential)]
public struct Branch
{
    public Int32 Number;
    public String Name;
    public String Address1;
    public String Address2;
    public String Address3;
    public String Town;
    public String County;
    public String Postcode;
    public Double Latitude;
    public Double Longitude;
    public String Telephone1;
    public String Email;
    public String BranchManager;
    public String OpeningHoursWeekend;
    public String OpeningHoursMonToFri;
    public String ClosingHoursWeekend;
    public String ClosingHoursMonToFri;
    public String ClosingHoursWeekDay;
    public List<Branch_Specialism> Specialisms;
}