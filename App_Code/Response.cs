﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Web;
using Newtonsoft.Json;

public class Response
{
    public Int32 StatusCode;
    public String Message;

    public override String ToString()
    {
        return JsonConvert.SerializeObject(this);
    }

    public static implicit operator String(Response response)
    {
        return response.ToString();
    }
}