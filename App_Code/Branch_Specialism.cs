﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Web;

[StructLayout(LayoutKind.Sequential)]
public struct Branch_Specialism
{
    public Int32 Id;
    public String Name;
    public Object Description;
}