﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <title>Jewson Development Test</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="base" Runat="Server">
    <div class="leftContent">
        <img src="/Images/Tree.jpeg" width="500"/>
    </div>
    <div class="rightContent">
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas tincidunt turpis sed dictum rutrum. Nunc at tempor nibh. Ut feugiat leo non ligula gravida laoreet. Cras ex augue, placerat nec ornare at, lacinia non est. Aliquam nec mi nibh. Ut elementum eros sit amet velit consectetur aliquet. Pellentesque semper sem lorem, vitae sagittis ligula bibendum non. Phasellus sodales pulvinar dui quis dignissim. Morbi rhoncus erat magna, nec ornare risus porttitor faucibus. Nulla dapibus faucibus blandit. Vestibulum tempor sem lectus, non dapibus nunc rutrum eu. Duis pulvinar sagittis dui sed accumsan. Nulla pharetra felis a lectus rhoncus vestibulum. Quisque eget sapien dignissim, ultrices erat ut, luctus est. Vivamus vestibulum pretium erat, non mollis dui ultricies eget.</p>

        <p>Etiam dictum lacus quis pharetra varius. Phasellus non nunc sit amet orci dapibus facilisis sit amet eget nisi. Vivamus vel augue ante. Ut venenatis ullamcorper ex non scelerisque. Proin ultrices nisi quam, vel lobortis eros iaculis sit amet. Mauris efficitur libero nec odio ultricies pharetra. Vestibulum feugiat, elit dictum laoreet mollis, leo erat molestie urna, ut commodo arcu massa non risus. Morbi dui enim, eleifend sed egestas et, vestibulum a lacus. Nunc augue erat, scelerisque et erat sed, consectetur egestas quam. Ut mattis purus est, vel accumsan nunc venenatis vitae. Aenean pharetra odio quis risus consequat, ac tincidunt massa tempus. Aenean lacinia viverra lacus, ac posuere ante volutpat eu. In vel feugiat velit. Vivamus a luctus mi. Fusce eros nibh, auctor eget varius ac, vehicula eu nulla. Nulla interdum suscipit magna sed mattis.</p>
    </div>
</asp:Content>

